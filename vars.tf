variable "region" {
    default = "North Europe"
}

variable "resource_group_name" {
  
}

variable "storage_account_name" {
  
}

variable "storage_account_key" {
  
}



variable "virtual_machine_name" {
  default = ""
}

variable "domain_name" {
  default = ""
}

variable "domain_adminuser" {
  default = ""
}

variable "domain_adminpass" {
  default = ""
}

variable "oms_workspace_id" {
  default = ""
}

variable "oms_workspace_key" {
  default = ""
}


variable "dcjoin" {
  default = "0"
}


variable "antimalware" {
    default = "0"
}

variable "iaasdiagnostics" {
  default = "0"
}

variable "oms" {
  default = "0"
}

