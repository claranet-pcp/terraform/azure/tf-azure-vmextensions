data "template_file" "xml" {
  template = "${file("${path.module}/includes/windows-config.xml.tmpl")}"
}

resource "azurerm_virtual_machine_extension" "iaas_diagnostics" {
  name                 = "iaas-diagnostics"
  count                = "${var.iaasdiagnostics}"
  location             = "${var.region}"
  resource_group_name  = "${var.resource_group_name}"
  virtual_machine_name = "${var.virtual_machine_name}"
  publisher            = "Microsoft.Azure.Diagnostics"
  type                 = "IaaSDiagnostics"
  type_handler_version = "1.5"

  settings = <<SETTINGS
    {
        "xmlCfg": "${base64encode(data.template_file.xml.rendered)}",
        "storageAccount": "${var.storage_account_name}"
    }
SETTINGS

  protected_settings = <<SETTINGS
    {
        "storageAccountName": "${var.storage_account_name}",
        "storageAccountKey": "${var.storage_account_key}"
    }
SETTINGS
}
