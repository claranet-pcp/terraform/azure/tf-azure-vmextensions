resource "azurerm_virtual_machine_extension" "dcjoin" {
  name                 = "dcjoin"
  count                = "${var.dcjoin}"
  location             = "${var.region}"
  resource_group_name  = "${var.resource_group_name}"
  virtual_machine_name = "${var.virtual_machine_name}"
  publisher            = "Microsoft.Compute"
  type                 = "JsonADDomainExtension"
  type_handler_version = "1.0"

  settings = <<SETTINGS
  {
    "Name": "${var.domain_name}",
    "User": "${var.domain_name}\\${var.domain_adminuser}",
    "Restart": "true",
    "Options" :  "3"
}
SETTINGS

  protected_settings = <<SETTINGS
{
  "Password":"${var.domain_adminpass}"
}
SETTINGS
}