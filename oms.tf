resource "azurerm_virtual_machine_extension" "oms_ext" {
  name                       = "oms-ext"
  count                      = "${var.oms}"
  location                   = "${var.region}"
  resource_group_name        = "${var.resource_group_name}"
  virtual_machine_name       = "${var.virtual_machine_name}"
  publisher                  = "Microsoft.EnterpriseCloud.Monitoring"
  type                       = "MicrosoftMonitoringAgent"
  type_handler_version       = "1.0"
  auto_upgrade_minor_version = true

  settings = <<SETTINGS
    {
      "workspaceId": "${var.oms_workspace_id}"
    }
SETTINGS

  protected_settings = <<SETTINGS
{
"workspaceKey": "${var.oms_workspace_key}"
}
SETTINGS
}